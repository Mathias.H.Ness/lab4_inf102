package INF102.lab4.median;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class QuickMedian implements IMedian {

    @Override
    public <T extends Comparable<T>> T median(List<T> list) {
        //List<T> listCopy = new ArrayList<>(list); // Method should not alter list
        
        int medianPos = list.size() / 2;
        return quickSelectMedian(list, medianPos);
    }

    /**
     * hjelpemetode sjekker elementer i lisiten og deler listen i en større enn pivot og mindre enn pivot og kjører
     * rekursivt på listen som inneholder posisjonen til medianen
     * @param <T>
     * @param list
     * @param medianPos
     * @return
     */
    private <T extends Comparable<T>> T quickSelectMedian(List<T> list, int medianPos) {
        if (list.size() == 1) return list.get(0); //bare ett elem, return elem

        List<T> smaller = new ArrayList<>(); //større/mindre
        List<T> larger = new ArrayList<>();

        T pivot = list.get(medianPos); //pivot, hvilket som helst elem i liste
        int pivotCounter = 0; //teller pivots, dersom flere er samme som pivot

        for (int i = 0; i < list.size(); i++) { //itererer gjennom liste
            T elem = list.get(i); //elem(i) større, mindre eller lik pivot

            if (elem.compareTo(pivot) < 0) smaller.add(elem);
            else if (elem.compareTo(pivot) == 0) pivotCounter++;
            else larger.add(elem);
        }

        if (smaller.size() > medianPos) return quickSelectMedian(smaller, medianPos); //smaller inneholder median
        if (smaller.size() + pivotCounter > medianPos) return pivot; //smaller + pivots, median er pivot
        return quickSelectMedian(larger, medianPos - smaller.size() - 1); // median er i larger listen

    }

}
