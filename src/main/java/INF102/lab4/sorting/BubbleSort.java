package INF102.lab4.sorting;

import java.util.List;

public class BubbleSort implements ISort {

    @Override
    public <T extends Comparable<T>> void sort(List<T> list) {
        for (int i = 0; i < list.size(); i++) { //itererer gjennom hvert element
            boolean swapped = false; //boolean som sier om elementet byttet plass
            for (int j = 0; j < list.size() - i - 1; j++) { // itererer gjennom hvert minus i, fordi bakerst i listen blir sortert først
                if (list.get(j).compareTo(list.get(j + 1)) > 0) { //sjekker om j er større enn j + 1
                    T temp = list.get(j); // ta ut en temporary og bytt plass
                    list.set(j, list.get(j + 1));
                    list.set(j + 1, temp);
                    swapped = true; //byttet
                }
            }
            if (!swapped) { //hvis den har gått gjennom hele listen en gang uten å bytte noe er listen sortert
                break;
            }
        }


    }
    
}
